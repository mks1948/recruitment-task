const express = require("express");

const UserCtrl = require("../user/userModel");

const router = express.Router();

router.post("/register", async (req, res) => {
	try {
		const user = new UserCtrl(req.body);
		await user.save();
		const token = await user.generateAuthToken();
		res.status(201).send({ user, token });
	} catch (err) {
		res.status(400).send(error);
	}
});
router.get("/user/registered", async (req, res) => {
	try {
		const user = await UserCtrl.findAllUsers();
		res.status(201).send({ user });
	} catch (err) {
		res.status(400).send(err);
	}
});
router.post("/user/login", async (req, res) => {
	try {
		const { email, password } = req.body;
		const user = await UserCtrl.findByCredentials(email, password);
		if (!user) {
			return res.status(401).send({ error: "Login failed !" });
		}
		const token = await UserCtrl.generateAuthToken();
		res.send({ user, token });
	} catch (error) {
		res.status(400).send(error);
	}
});
router.post("/users/remove", async (req, res) => {
	try {
		const { id } = req.body;
		await UserCtrl.removeUser(id);
		const finalUpdate = await UserCtrl.findAllUsers();
		res.status(201).send({ finalUpdate });
	} catch (err) {
		res.status(400).send(err);
	}
});
module.exports = router;

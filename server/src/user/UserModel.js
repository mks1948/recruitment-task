const mongoose = require("mongoose");
const validator = require("validator");
const jwt = require("jsonwebtoken");

const userSchema = mongoose.Schema({
	name: {
		type: String,
		required: true,
		trim: true
	},
	surname: {
		type: String
	},
	description: {
		type: String
	},
	cats: {
		type: String
	},
	email: {
		type: String,
		required: true,
		unique: true,
		lowercase: true,
		validate: (value) => {
			if (!validator.isEmail(value)) {
				throw new Error({ error: "Invalid Email address" });
			}
		}
	},
	tokens: [
		{
			token: {
				type: String,
				required: true
			}
		}
	]
});

userSchema.pre("save", async function(next) {
	next();
});

userSchema.methods.generateAuthToken = async function() {
	// Generate an auth token for the user
	const user = this;
	const token = jwt.sign({ _id: user._id }, "process.env.JWT_KEY");
	user.tokens = user.tokens.concat({ token });
	await user.save();
	return token;
};

userSchema.statics.findByCredentials = async (email, password) => {
	const user = await User.findOne({ email });
	if (!user) {
		throw new Error({ error: "Invalid login credentials" });
	}
	return user;
};
userSchema.statics.findAllUsers = async () => {
	const user = await User.find({});
	return user;
};
userSchema.statics.removeUser = async (id) => {
	const user = await User.deleteOne({ _id: `${id}` });
	return user;
};
const User = mongoose.model("User", userSchema);
module.exports = User;

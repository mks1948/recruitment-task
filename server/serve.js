const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const path = require("path");

const db = require("./src/db/index");
const userRoute = require("./src/router/router");

const app = express();
const apiPort = 3001;

app.use(
	bodyParser.urlencoded({
		extended: true
	})
);
app.use(cors());
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, "../build")));
if (process.env.NODE_ENV === "production") {
	app.use(express.static("../client/build"));

	app.get("*", (request, response) => {
		response.sendFile(
			path.resolve(__dirname, "..", "build", "index.html")
		);
	});
}
// res.sendFile(path.join(__dirname + "../client/build/index.html"));

db.on("error", console.error.bind(console, "MongoDB connection error:"));

app.use(userRoute);

app.listen(apiPort, () => console.log(`Server running on port ${apiPort}`));

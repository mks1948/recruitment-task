import React, { useState, useEffect, useCallback } from 'react';
import axios from 'axios'
import ListUser from '../ListUsers/ListUsers'
import Button from '@material-ui/core/Button';
import { useHistory } from 'react-router-dom'

const ShowUsers = () => {
  const [data, setData] = useState([]);
  const [loaded, setLoaded] = useState(false)
  const history = useHistory()
  const handleLogout = useCallback(() => {
    localStorage.removeItem('auth')
    history.push('/')
  }, [history])
  useEffect(() => {
    if (localStorage.getItem('auth') !== null || undefined) {
      axios.get('http://localhost:3001/user/registered').then((res) => {
        setData(res.data.user)
        setLoaded(true)
      })
        .catch(() => {
          handleLogout();
        })
    } else {
      handleLogout();
    }
  }, [handleLogout])

  const handleRemoveItem = (id) => {
    setLoaded(false)
    axios.post('http://localhost:3001/users/remove', { 'id': id }).then((
      res
    ) => {
      setData(res.data.finalUpdate)
      setLoaded(true)
      if (res.data.finalUpdate.length === 0) {
        handleLogout()
      }
    }).catch((err) => {
      console.log('error remove', err)
    })
  }

  return (
    <>
      <Button variant="contained" style={{ margin: '1rem' }} onClick={handleLogout}>
        {!loaded ? 'Back' : 'Logout'}
      </Button>
      {loaded && data.map((item) => {
        const { name, surname, _id, description, cats } = item
        return (<ListUser key={_id} handleRemoveItem={handleRemoveItem} id={_id} name={name} surname={surname} cats={cats} description={description} />)
      })}
    </>
  )

}
export default ShowUsers
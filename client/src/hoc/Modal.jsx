import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';


const useStyles = makeStyles(theme => ({
  paper: {
    position: 'absolute',
    zIndex: '1500',
    minWidth: 320,
    maxWidth: 400,
    height: '10rem',
    right: `0%`,
    left: `0%`,
    marginLeft: 'auto',
    top: '0%',
    bottom: '0%',
    marginTop: 'auto',
    marginBottom: 'auto',
    marginRight: 'auto',
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function ModalComponent(props) {
  const classes = useStyles();
  const { open, onClose } = props
  return (
    <Modal
      open={open}
      onClose={onClose}
    >
      <div className={classes.paper}>
        {props.children}
      </div>
    </Modal >
  );
}

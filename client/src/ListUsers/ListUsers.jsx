import React, { useState } from 'react'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { makeStyles } from '@material-ui/core/styles';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import ModalComponent from '../hoc/Modal'
import Button from '@material-ui/core/Button';
const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
  green: {
    color: 'green',
    fontWeight: 700,
  },
  containerButton: {
    display: 'flex',
    justifyContent: 'space-around',
  },
  text: {
    minWidth: 100,
  }
}));

const ListUser = ({ name, surname, cats, description, id, handleRemoveItem }) => {
  const classes = useStyles();
  const [displayModal, setDisplayModal] = useState(false)

  const handleToggleModal = () => {
    setDisplayModal(!displayModal)
  }
  const list = (
    <List className={classes.root} onDoubleClick={handleToggleModal}>
      <ListItem button>
        <ListItemText
          className={classes.text}
          primary={
            <>
              <Typography
                className={cats.length > 0 ? classes.green : ''}
              >
                {name}
              </Typography>
            </>} secondary={surname} />
        <ListItemText className={classes.text}
          primary={cats.length > 0 ? cats : 'x'} />
        <ListItemText className={classes.text}
          primary={description.length > 0 ? description : 'x'} />
      </ListItem>
    </List>
  )
  return (
    <>
      {list}
      {
        displayModal &&
        <ModalComponent onClose={handleToggleModal} open={true}>
          <div>
            <h3>Do you remove user : {name} {surname}</h3>
            <div className={classes.containerButton}>
              <Button variant="outlined" color="primary" onClick={() => handleRemoveItem(id)} >
                Yes
              </Button>
              <Button variant="outlined" onClick={handleToggleModal} color="secondary">
                No
              </Button>
            </div>
          </div>
        </ModalComponent>
      } </>

  )
}
export default ListUser
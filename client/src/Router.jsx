import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Register from './Register/Register';
import ShowUsers from './ShowUsers/ShowUsers';
import NotFound from './NotFound/NotFound';

const RouterComp = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Register} />
        <Route path="/user" component={ShowUsers} />
        <Route component={NotFound} />
      </Switch>
    </BrowserRouter>
  )
}
export default RouterComp
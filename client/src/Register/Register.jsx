import React, { useState } from 'react';
import styled from 'styled-components';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Checkbox from '@material-ui/core/Checkbox';
import axios from 'axios';
import { useHistory } from 'react-router-dom'

const Input = styled.input`
  margin: 0.5em;
  padding: 1em;
  background-color: ${props => props.theme.main};
  border: none;
  border-radius: 3px;
  font-size: 18px;
`;
const Header = styled.h1`
  text-align: center;
`;
const ConfirmationEmailField = styled.div`
  display:flex;
`;
const ContainerLogin = styled.form`
display: flex;
flex-direction: column;
min-width: 320px;
margin: 3rem auto;
max-width: 520px;
  `
const Button = styled.button`
  width: 100%;
  color: #fff;
  border-color: #2e6da4;
  padding: 1em;
  margin-top: 1em;
  border-radius: 3px;
  border: none;
  background: ${props => props.theme.main};
  `

const Register = () => {
  const history = useHistory()
  const [state, setState] = useState({
    email: '',
    description: '',
    confirmationEmail: false,
    name: '',
    surname: '',
  });
  const [cats, setCats] = React.useState('');
  const [check, setCheck] = React.useState({
    checkedYes: false,
    checkedNo: false,
  });

  const { email, name, surname, description, confirmationEmail } = state;
  const { checkedYes, checkedNo } = check;
  const handleChangeInput = name => e => {
    if (name !== 'confirmationEmail') {
      setState({
        ...state,
        [name]: e.target.value
      });
    }
    else {
      setState({
        ...state,
        [name]: e.target.checked
      })

    }
  };
  const checkFilledField = () => {
    const checkFill = () => {
      return (
        surname.length > 3 &&
        name.length > 3 &&
        email.length > 5
      );
    }
    const checked = checkFill();
    if (cats.length === 0) {
      return checked && description.length > 10
    }
    else {
      return checked
    }
  };
  const handleSubmit = e => {
    e.preventDefault();
    const matches = surname.match(/\b\w*DR\w*\b/);
    if (matches != null) {
      const send = {
        name,
        surname,
        email,
        description,
        cats
      };
      axios
        .post('http://localhost:3001/register', send)
        .then(res => {
          localStorage.setItem('auth', res.data.token)
          history.push('/user');
        })
        .catch(() => {
          alert('Check password , email or write surname with surname "DR"')
        });
    }
    else {
      alert('Check form')
    }
  };

  const handleChangeCheck = name => event => {
    setCats('');
    setState({
      ...state,
      description: '',
    })
    setCheck({ [name]: event.target.checked });
  };

  const handleChangeDrop = event => {
    setCats(event.target.value);
  };
  const describe = (
    <Input
      required
      value={description}
      placeholder="Why don’t you like cats?"
      onChange={handleChangeInput('description')}
      theme={description.length > 10 ? { main: '#7faaf0' } : { main: '#f27a72' }}
      label="Describe"
    />
  )
  const selectCats = (
    <>
      <InputLabel >Select Cats</InputLabel>
      <Select
        value={cats}
        onChange={handleChangeDrop}
      >
        <MenuItem value={'Abyssinian'}>Abyssinian</MenuItem>
        <MenuItem value={'Bengal'}>Bengal cat</MenuItem>
        <MenuItem value={'Birman'}>Birman</MenuItem>
      </Select>
    </>
  )
  return (
    <>
      <Header>Register</Header>
      <ContainerLogin onSubmit={e => handleSubmit(e)}>
        <Input
          required
          theme={name.length > 3 ? { main: '#7faaf0' } : { main: '#f27a72' }}
          type="text"
          placeholder="Name"
          value={name}
          onChange={handleChangeInput('name')}
        />
        <Input
          required
          theme={surname.length > 3 ? { main: '#7faaf0' } : { main: '#f27a72' }}
          type="text"
          placeholder="Surname"
          value={surname}
          onChange={handleChangeInput('surname')}
        />
        <Input
          required
          type="email"
          placeholder="Email"
          theme={email.length > 5 ? { main: '#7faaf0' } : { main: '#f27a72' }}
          value={email}
          autocomplete="off"
          onChange={handleChangeInput('email')}
        />
        Do you like cat ?
        <FormControlLabel control={<Checkbox value="checkedYes" checked={checkedYes} onChange={handleChangeCheck('checkedYes')}
        />} label="Yes" />
        <FormControlLabel control={<Checkbox value="checkedNo" checked={checkedNo} onChange={handleChangeCheck('checkedNo')}
        />} label="No" />
        {checkedYes && selectCats}
        {checkedNo && describe}
        <ConfirmationEmailField>
          <>
            <p>Do you want to receive registration confirmation email?</p>
            <Checkbox
              checked={confirmationEmail}
              value={confirmationEmail}
              onChange={handleChangeInput('confirmationEmail')}
            />
          </>
        </ConfirmationEmailField>
        <Button
          style={{ width: '100%' }}
          disabled={!checkFilledField()}
          onSubmit={e => handleSubmit(e)}
          theme={
            !checkFilledField()
              ? { main: 'linear-gradient(to right, #757f9a, #d7dde8)' }
              : { main: 'linear-gradient(to right, #11998e, #38ef7d)' }
          }
        >
          Register
        </Button>
      </ContainerLogin>
    </>
  );
};
export default Register;

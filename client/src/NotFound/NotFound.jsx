import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { useHistory } from 'react-router-dom'

const useStyles = makeStyles(() => ({
  content: {
    textAlign: 'center'
  },
  container: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    width: '50%',
    margin: '0 auto'
  }
}));

const NotFound = () => {
  const classes = useStyles();
  const history = useHistory()

  return (
    <div className={classes.container}>
      <h1 className={classes.content}>Page not found</h1>
      <Button variant="outlined" color="primary" onClick={() => history.push('/')} >
        Back to home page
    </Button>
    </div>
  )
}
export default NotFound